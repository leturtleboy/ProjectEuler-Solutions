def myfunc(n):
  return lambda a : a * n

f = myfunc(2)
print(f(3))