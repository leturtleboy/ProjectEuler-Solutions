"""
9! < 999999 < 10!

2*! < 999999

so

725760 < 999999
2*9! + 6*8! + 6*7! < 999999

2*9! means that we start whit 2

[2 0 1 3 4 5 6 7 8 9]

6*8! means that we start the aux vector[2 [7,0,1,3,4,5,6,8,9]]

6*7! means that we start the aux vector[2 [7 [8,0,1,3,4,5,6,9]]]

so mi starter vector is gonna be [2,7,8,0,1,3,4,5,6,9] and is the n = 997920 permutation
"""

def next_num_permutation(arr):
	i = len(arr) - 1
	# Encontrar las coordenadas del primer elemento menor a mover
	while i > 0 and arr[i - 1] >= arr[i]:
		i -= 1

	if i <= 0:
		return False, arr

	j = len(arr) - 1
	while arr[j] <= arr[i - 1]:
		j -= 1

	arr[i - 1], arr[j] = arr[j], arr[i - 1]
	arr[i : ] = arr[len(arr) - 1 : i - 1 : -1]

	return True, arr

n = 997920
m = 999999
state = True
vector = [2,7,8,0,1,3,4,5,6,9]
for i in range(n,m,1):
	state, vector = next_num_permutation(vector)
	if not state:
		break
print("".join([str(x) for x in vector]))
