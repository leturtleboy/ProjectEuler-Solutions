import functools
import math

abundants = {}
def divisorGenerator(n): #Generator
    large_divisors = []
    for i in range(1, int(math.sqrt(n) + 1)):
        if n % i == 0:
            yield i
            if i*i != n:
                large_divisors.append(n / i)
    for divisor in reversed(large_divisors):
        if divisor != n:  # evit to return the same number
            yield int(divisor)

sol = []
for n in range (1,28124):
    sum_of_div = sum([i for i in divisorGenerator(n)])
    if n < sum_of_div:
        abundants[n] = [sum_of_div]
abundant_list = sorted( list( abundants.keys() ) )

numbers = []
for i in range(len(abundant_list)):
    for j in range(i,len(abundant_list)):
        generated = abundant_list[i]+abundant_list[j]
        if (generated > 28124):
            break
        else:
            if (generated in numbers):
                pass
            else:
                numbers.append(generated)
solution = 0
for i in range(28124):
    if i in numbers:
        pass
    else:
        solution+=i
print(solution)