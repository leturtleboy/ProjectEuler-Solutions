import functools
import math

dic = {}
def divisorGenerator(n): #Generator
    large_divisors = []
    for i in range(1, int(math.sqrt(n) + 1)):
        if n % i == 0:
            yield i
            if i*i != n:
                large_divisors.append(n / i)
    for divisor in reversed(large_divisors):
        if divisor != n:
            yield int(divisor)

sol = []
for n in range (1,10000):
    amicable = sum([i for i in divisorGenerator(n)])
    comparer = sum([j for j in divisorGenerator(amicable)])
    if (n == comparer) & (n != amicable):
        dic[n] = amicable
print(sum(dic.keys()))