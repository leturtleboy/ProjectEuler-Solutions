import functools
def factorial(numero):
    '''
    This lambda function returns the factorial of numero var.
    reduce ( function, list of numbers)
    
    Example
    numero = 4.

    First Reduction:
    x = 0


    '''
    return functools.reduce(lambda x, y: x*y, list(range(1,numero+1))) 

def suma_car_factorial(numero):
     n = list(map(int, list(str(factorial(numero)))))
     return functools.reduce(lambda x, y : x+y,n)
    
print(suma_car_factorial(100))