import numpy as np

def get_value(name):
    real_value = 0
    for i in name:
        real_value += (ord(i) - 64)
    return real_value
        

triFile = open('problems_20_to_29\\p022_names.txt')
names = []
 
with triFile as myfile:
    names=triFile.read().replace("\"","")
names = names.split(',')
names = sorted(names, key=str.lower)
value = np.zeros(len(names))
for i in range(len(names)):
    val = get_value(names[i])
    value[i] = (i+1)*val
print(sum(value))
    
