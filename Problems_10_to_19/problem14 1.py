import time as time
a = time.time()
max = [0,0]
for i in range(1,10000):
    n = i
    count = 0
    while True:
        if n != 1:
            if n%2 == 0:
                n = n/2
            else:
                n = 3*n + 1
            count += 1
        else: 
            if max[0] < count:
                max[0] = count
                max[1] = i
            break
print(max)
print(time.time() - a)