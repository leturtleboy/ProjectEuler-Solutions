import numpy as np
triFile = open('Problems_10_to_19\\triangle.txt')
triangle = []
 
for num in triFile:
    triangle.append([ int(x) for x in num.strip().split() ])

aux = list(range(1,len(triangle)))
for i in aux:
    aux2 = list(range(len(triangle[i])))
    for j in aux2:
        if j == 0:
            triangle[i][j] = triangle[i][j] + triangle[i-1][j]
        elif j == len(triangle[i-1]):
            triangle[i][j] = triangle[i][j] + triangle[i-1][j-1]
        else:
            triangle[i][j] = max(triangle[i][j] + triangle[i-1][j],triangle[i][j] + triangle[i-1][j-1])
 
print(max(triangle[len(triangle)-1]))
