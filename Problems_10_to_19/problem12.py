def tau(num):
    n = num
    i = 2
    p = 1
    divisores = [1]
    if (num == 1): return 1 # Si el numero es 1 solo tiene un divisor

    while (i * i <= n): # preguntamos por los numeros primos que lo conforman hasta la raiz del numero objetivo.
        c = 1
        while ((n % i) == 0):
            n /= i # reduzco el numero objetivo
            c+=1 # aumento en 1 la cantidad de veces qie divido por ese numero
            divisores.append(i)
        i+=1 # Busco el siguiente numero
        p*= c # Aumento la cantidad de combinaciones posibles con el numero de divisores que tengo

    if (n == num) | (n > 1): # Si es primo el numero o me falta un numero por evaluar, es porque tengo un primo mas por meter.
        if (n == num): divisores.append(num)
        else: divisores.append(n)
        p*= 2
    if len(divisores) < p : divisores.append(num)
    return p #, divisores

u = 1
while True:
    l = sum([n for n in range(u)])
    number = tau(l)
    if number >= 500:
        print("lo encontre: ")
        print(l)
        break
    u+=1