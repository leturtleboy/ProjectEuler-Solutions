import time as time

dic = {1: [1, 0], 2: [1, 1]}


def confirmar(num):
    if not(num in dic):
        n = num
        if n % 2 == 0:
            n = int(n/2)
        else:
            n = int(3*n + 1)
        if n in dic:
            dic[num] = [n, dic[n][1]+1]
        else:
            confirmar(n)
            dic[num] = [n, dic[n][1]+1]


a = time.time()
for i in range(1, 1000000):
    confirmar(i)
# How to sort a Python dict by value
# (== get a representation sorted by value)
print(sorted(dic.items(), key=lambda x: x[1][1])[-1])
print(time.time() - a)

