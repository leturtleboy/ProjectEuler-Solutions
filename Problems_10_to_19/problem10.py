k = 2000000 #limit
#find primes until 2000000

def primes(n):
    if n<=2:
        return []
    sieve=[True]*(n+1)
    limite_sup = int(n**0.5)+1
    for x in range(3,limite_sup,2):
        limite_inf = (n//x)+1
        for y in range(3,limite_inf,2):
            sieve[(x*y)]=False

    return [2]+[i for i in range(3,n,2) if sieve[i]]

prime_list = primes(k)
print(sum(prime_list))