#Problem 1 Project Euler

print(sum([x for x in range(1000) if ((x%3 == 0) or (x%5 == 0))]))

'''
# Equivalent to
list = []
for i in range(1000):
    if ((i%3 == 0) | (i%5 == 0)):
        list.append(i)
print(sum(list))
'''