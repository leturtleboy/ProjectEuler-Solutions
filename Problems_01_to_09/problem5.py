'''
two = 2
tree = 3
four = 2*2
five = 5
six = 2 * 3
seven = 7
eight = 2*2*2
nine = 3*3
ten = 5 * 2
eleven = 11
twelve = 3*2*2
trerdteen = 13
fourteen = 7 * 2
fifteen = 5 * 3
sixteen = 2*2*2*2
seventeen = 17
eightteen = 3*3*2
nineteen = 19
tweenty = 5*2*2
'''

# particular solution
number = 2*2*2*2*3*3*5*7*11*13*17*19
print(number)


k = 20 #limit
#find primes until 20

def is_prime(n):
    if n == 2:
        return True
    if n % 2 == 0 or n < 2:
        return False
    limit = int(n ** 0.5) + 1
    for i in range(3, limit, 2):
        if n % i == 0:
            return False
    return True

def primes_until(limit):
    primes = [2]
    count = 1
    n = 3
    while True:
        if is_prime(n):
            count += 1
            if n >= limit:
                return primes
            primes.append(n)
        n += 2

prime_list = primes_until(k)

definitive = prime_list[:]

def fullfill_list():
    for i in prime_list:
        b = i
        while True:
            if b*i < k:
                definitive.append(i)
                b = i*b
            else:
                break

fullfill_list()
solution =  1
for x in definitive:
    solution = solution*x
print("Solution: ",solution)